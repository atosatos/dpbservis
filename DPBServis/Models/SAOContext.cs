﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;


// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace DPBServis.Models
{
    public partial class SAOContext : DbContext
    {

        public SAOContext(DbContextOptions<SAOContext> options)
            : base(options)
        {
        }
        public virtual DbSet<Output> Output { get; set; }
        public virtual DbSet<InputCaseIdsKJS> InputCaseIdsKSJ { get; set; }
        public virtual DbSet<LinkKJS> LinkKSJ { get; set; }
        public virtual DbSet<CaseDisputeKJS> CaseDisputeKSJ { get; set; }
        public virtual DbSet<DocumentKJS> DocumentKJS { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<InputCaseIdsKJS>().HasNoKey();
            modelBuilder.Entity<LinkKJS>().HasNoKey();
            modelBuilder.Entity<CaseDisputeKJS>().HasNoKey();

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);


    }
}
