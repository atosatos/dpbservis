﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DPBServis.Models
{
    public class DocumentKJS
    {
        public long Id { get; set; }
        public long? CaseId { get; set; }
        public long? ContentId { get; set; }
        public string ContentLocation { get; set; }
    }
}
