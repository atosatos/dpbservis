﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DPBServis.Models
{
    public class LinkKJS
    {
        public long CaseId { get; set; }
        public int ExternalEntityId { get; set; }
        public string ExternalNum { get; set; }
        public string ExternalEntity { get; set; }
    }
}
