﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DPBServis.Models
{
    public class CaseDisputeKJS
    {
        public long CaseId { get; set; }
        public int DisputeId { get; set; }
        public string Dispute { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }

    }
}
