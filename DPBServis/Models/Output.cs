﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DPBServis.Models
{
    public class Output
    {

        public long ID { get; set; }
        public long CaseId { get; set; }
        public string CaseNum { get; set; }
        public string JBKJS { get; set; }
        public string JBKJSName { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? ReceiveDate { get; set; }
        public long? DecisionId { get; set; }
        public int? DecisionTypeId { get; set; }
        public string DecisionType { get; set; }
        public DateTime? DecisionDate { get; set; }
        public byte? DecisionLevelId { get; set; }
        public string DecisionLevel { get; set; }
        public DateTime? ValidDate { get; set; }
        public decimal? DecisionAmount { get; set; }
        public string DecisionCurrency { get; set; }
        public long? DecisionDocumentId { get; set; }
        public int? CourtLinkEntityId { get; set; }
        public string CourtLinkEntity { get; set; }
        public string CourtLinkNum { get; set; }
       // public int? CaseStatusId { get; set; }
        public string CaseStatus { get; set; }
        [NotMapped]
        public List<LinkKJS> LinkKSJs { get; set; }
        [NotMapped]
        public List<CaseDisputeKJS> CaseDisputeKSJs { get; set; }

    }
}
