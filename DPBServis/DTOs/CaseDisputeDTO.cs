﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DPBServis.Models;

namespace DPBServis.DTOs
{
    public class CaseDisputeDTO
    {
        public long CaseId { get; set; }
        public int DisputeId { get; set; }
        public string Dispute { get; set; }
        public decimal Amount { get; set; }
    }
}
