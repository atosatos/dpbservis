﻿using DPBServis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DPBServis.DTOs;

namespace DPBServis.DTOs
{
    public class CaseFileDTO
    {
        public CaseFileDTO(Output output)
        {
            CaseNum = output.CaseNum;
            CreationDate = output.CreationDate;
            DecisionType = output.DecisionType;
            DecisionLevel = output.DecisionLevel;
            DecisionDate = output.DecisionDate;
            ValidDate = output.ValidDate;
            DecisionAmount = output.DecisionAmount;
            JBKJS = output.JBKJS;
            CourtLinkEntity = output.CourtLinkEntity;
            CourtLinkNum = output.CourtLinkNum;

        }
        public long CaseId { get; set; }
        public string CaseNum { get; set; }
        public DateTime? CreationDate { get; set; }
        public long? DecisionId { get; set; }
        public int? DecisionTypeId { get; set; }
        public string DecisionType { get; set; }
        public DateTime? DecisionDate { get; set; }
        public byte? DecisionLevelId { get; set; }
        public string DecisionLevel { get; set; }
        public DateTime? ValidDate { get; set; }
        public decimal? DecisionAmount { get; set; }
        public string JBKJS { get; set; }
        public long? DecisionDocumentId { get; set; }
        public int? CourtLinkEntityId { get; set; }
        public string CourtLinkEntity { get; set; }
        public string CourtLinkNum { get; set; }

        public List<LinkDTO> Links { get; set; }
        public List<CaseDisputeDTO> CaseDisputes { get; set; }
    }
}
