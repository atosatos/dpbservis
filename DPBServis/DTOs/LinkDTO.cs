﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DPBServis.Models;

namespace DPBServis.DTOs
{
    public class LinkDTO
    {
        public int ExternalEntityId { get; set; }
        public string ExternalEntity { get; set; }
        public string ExternalCaseNum { get; set; }
    }
}
