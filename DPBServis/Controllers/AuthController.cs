﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DPBServis.DTOs;
using Microsoft.Data.SqlClient;
using DPBServis.Services;
using Microsoft.AspNetCore.Authorization;
using DPBServis.Models;

namespace DPBServis.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController: ControllerBase
    {
        private IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

       

        [HttpGet]
        public ActionResult Auth()
        {
            var token = _authService.Auth();
            if (token == null || token == string.Empty)
                return BadRequest(new { message = "User name or password is incorrect" });

            return Ok(token);
        }
    }
}
