﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DPBServis.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DPBServis.Services;

namespace DPBServis.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentKJSController : ControllerBase
    {
        private IDocumentService _documentService;
        public DocumentKJSController(IDocumentService documentService)
        {
            _documentService = documentService;
        }

        // GET: api/DocumentKJS
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DocumentKJS>>> GetDocumentKJS()
        {

            return Ok(await _documentService.GetDocumentKJS());
        }

        // GET: api/DocumentKJS/5
        [HttpGet("{CaseId}")]
        public IActionResult GetDocumentKJS(long caseId)
        {

            return _documentService.GetDocumentKJS(caseId);

        }


    }
}
