﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DPBServis.DTOs;
using Microsoft.Data.SqlClient;
using DPBServis.Services;
using Microsoft.AspNetCore.Authorization;
using DPBServis.Models;

namespace DPBServis.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CaseFilesController : ControllerBase
    {
        private ICasefileService _casefileService;

        public CaseFilesController(ICasefileService casefileService)
        {
            _casefileService = casefileService;
        }

        [HttpGet]
        public ActionResult<List<Output>> GetCasefileDataKSJ(string date)
        {

            List<Output> list = _casefileService.GetCasefileDataKSJ(date);

            return Ok(list);
        }

    }
}
