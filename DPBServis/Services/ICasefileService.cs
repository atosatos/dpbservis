﻿using DPBServis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DPBServis.Services
{
    public interface ICasefileService
    {
        List<Output> GetCasefileDataKSJ(string date);

    }
}
