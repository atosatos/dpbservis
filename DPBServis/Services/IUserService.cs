﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DPBServis.Services
{
    public interface IUserService
    {
        string Login(string userName, string password);
        string Auth();
    }
}
