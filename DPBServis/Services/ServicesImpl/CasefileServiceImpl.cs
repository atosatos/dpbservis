﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Microsoft.Data.SqlClient;
using DPBServis.Models;
using DPBServis.Services;

namespace DPBServis.Services.ServicesImpl
{
    public class CasefileServiceImpl : ICasefileService
    {

        private readonly SAOContext _context;

        public CasefileServiceImpl(SAOContext context)
        {
            _context = context;
        }
        public List<Output> GetCasefileDataKSJ(string date)
        {
            string spInput = $"exec GetCaseIdsKSJ @Date='{date}'";

            List<InputCaseIdsKJS> listInput = _context.InputCaseIdsKSJ.FromSqlRaw(spInput).ToList();

            DataTable dtInput = new DataTable();
            dtInput.Columns.Add("CaseId", typeof(long));

            foreach (InputCaseIdsKJS i in listInput)
            {
                dtInput.Rows.Add(new object[] { i.CaseId });
            }

            var parameter = new SqlParameter("@CaseFiles", SqlDbType.Structured);
            parameter.Value = dtInput;
            parameter.TypeName = "[dbo].[UTCaseId]";

            // string storedProcedure = $"exec GetCaseFileDataKSJ @CaseFiles='{dtInput}'";
            string spCaseFiles = $"exec GetCaseFileDataKSJ @CaseFiles";

            List<Output> list = _context.Output.FromSqlRaw(spCaseFiles, parameter).ToList();

            // var list = _context.Output.FromSql("EXEC [dbo].[GetCaseFileDataKSJ] @CaseFiles", parameter).ToList();

            string spLinks = $"exec GetLinksKSJ @CaseFiles";

            List<LinkKJS> listLinks = _context.LinkKSJ.FromSqlRaw(spLinks, parameter).ToList();

            string spCaseDisputes = $"exec GetCaseDisputesKSJ @CaseFiles";

            List<CaseDisputeKJS> listCaseDisputes = _context.CaseDisputeKSJ.FromSqlRaw(spCaseDisputes, parameter).ToList();

            foreach (Output cf in list)
            {
                cf.LinkKSJs = listLinks.Where(l => l.CaseId == cf.CaseId).ToList();
                cf.CaseDisputeKSJs = listCaseDisputes.Where(c => c.CaseId == cf.CaseId).ToList();
            }

            return list;
        }
    }
}
