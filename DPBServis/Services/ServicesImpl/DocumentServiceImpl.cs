﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DPBServis.Models;
using DPBServis.Services;

namespace DPBServis.Services.ServicesImpl
{
    public class DocumentServiceImpl : IDocumentService
    {
        private readonly SAOContext _context;

        public DocumentServiceImpl(SAOContext context)
        {
            _context = context;
        }

        public async Task<List<DocumentKJS>> GetDocumentKJS()
        {
            return await _context.DocumentKJS.ToListAsync();
        }

        public IActionResult GetDocumentKJS(long caseId)
        {
            string contentLocation = "";
            string fileName = "";
            string spInput = $"exec GetDecisionContentKJS @CaseId='{caseId}'";

            //poziv proceduri koja vraca value pogledati
            List<DocumentKJS> documentList = _context.DocumentKJS.FromSqlRaw(spInput).ToList();
            if (documentList != null && documentList.Count > 0)
            {
                contentLocation = documentList[0].ContentLocation;
                fileName = contentLocation.Substring(contentLocation.LastIndexOf('/') + 1);

                var client = new System.Net.WebClient();
                //proveriti autentifikaciju za pdf
                client.Credentials = new System.Net.NetworkCredential("LurisHod", "123qwe!@#QWE");

                var data = client.DownloadData(contentLocation);
                return new FileContentResult(data, "application/pdf")
                {
                    FileDownloadName = "Odluka_" + fileName
                };
            }
            else
            {
                return new EmptyResult();
            }


        }
    }
}
