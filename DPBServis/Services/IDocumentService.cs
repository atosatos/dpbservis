﻿using DPBServis.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DPBServis.Services
{
    public interface IDocumentService
    {
        Task<List<DocumentKJS>> GetDocumentKJS();
        IActionResult GetDocumentKJS(long caseId);
    }
}
